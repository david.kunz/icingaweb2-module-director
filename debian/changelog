icingaweb2-module-director (1.11.3-1) unstable; urgency=medium

  * Merging upstream version 1.11.3.
  * Remove unnecessary entries in rules.
  * Updating copyright for d/.
  * Adding Brazilian Portuguese debconf translations from Paulo Henrique
    de Lima Santana (Closes: #1094250).
  * Updating Standards-Version to 4.7.0.
  * Adding Portuguese debconf translations from Américo Monteiro
    (Closes: #1099376).

 -- David Kunz <david.kunz@dknet.ch>  Thu, 06 Mar 2025 09:18:18 +0100

icingaweb2-module-director (1.11.1-1) unstable; urgency=medium

  * Adding Swedish debconf translations from Martin Bagge (Closes: #1059113).
  * Adding French debconf translations from Jean-Pierre Giraud
    (Closes: #1059862).
  * Updating German debconf translation from Hermann-Josef Beckers
    (Closes: #1065083).
  * Renaming d/po/ge.po to d/po/de.po to correct language code.
  * Correcting debconf question (Closes: #1052179).
  * Unfuzzy d/po/*.po files because the change is small.
  * Updating Spanish debconf translations from Camaleón.
  * Removing unnecessary line 'X-Generator' in d/po/{es,fr,de,nl}.po files.
  * Merging upstream version 1.11.1 (Closes: #1057902).
  * Updating copyright for 2024.
  * Updating copyright for Icinga Development Team.
  * Correcting misspelling in changelog for 1.10.2-{1,2}.

 -- David Kunz <david.kunz@dknet.ch>  Mon, 25 Mar 2024 15:02:40 +0100

icingaweb2-module-director (1.10.2-2) unstable; urgency=medium

  * Configure debconf.
  * Adding Spanish debconf translations from Camaleón.
  * Adding Dutch debconf translations from Francs Spiesschaert.
  * Adding German debconf translations from Christoph Brinkhaus.
  * Adding upstream metadata.
  * Updating copyright for debian directory.
  * Standardization of the rules for icingaweb2 modules.
  * Standardization of the control file for icingaweb2 modules.

 -- David Kunz <david.kunz@dknet.ch>  Fri, 25 Aug 2023 08:00:44 +0200

icingaweb2-module-director (1.10.2-1) unstable; urgency=high

  * Merging upstream version 1.10.2.
  * Removing patch to split.
  * Removing redhat entries in icinga-director.service.
  * Adding condition to icinga-director.service.
  * Changing icinga-director.service user and group according debian standard.
  * Changing ipl to php-library.
  * Remove duplicated field from control.
  * Updating Standards-Version to 4.6.2.
  * Adding Rules-Requires-Root in control.
  * Remove unused doc files.
  * Removing unused BSD-3 clause in copyright.
  * Updating delimiter in Icinga2Agent.bash confirm to debian policy.

 -- David Kunz <david.kunz@dknet.ch>  Tue, 07 Feb 2023 12:43:53 +0100

icingaweb2-module-director (1.8.1-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * No source change upload to rebuild with debhelper 13.10.

 -- Michael Biebl <biebl@debian.org>  Sat, 15 Oct 2022 12:08:58 +0200

icingaweb2-module-director (1.8.1-2) unstable; urgency=medium

  * Updating Standards-Version to 4.6.0.
  * Remove library/vendor/php-diff/ from copyright.

 -- David Kunz <david.kunz@dknet.ch>  Wed, 22 Dec 2021 11:17:56 +0100

icingaweb2-module-director (1.8.1-1) unstable; urgency=medium

  * Adding watch file.
  * Updating copyright for David Kunz.
  * Updating vcs in control.
  * Merging upstream version 1.8.1.
  * Updating copyright for Icinga Development Team.
  * Changing depends in control.
  * Removing .pc files from binary package.

 -- David Kunz <david.kunz@dknet.ch>  Thu, 22 Jul 2021 23:31:21 +0200

icingaweb2-module-director (1.8.0-1) unstable; urgency=medium

  * Merging upstream version.
  * Updating standard version.
  * Adding service.

 -- David Kunz <david.kunz@dknet.ch>  Thu, 05 Jan 2021 13:30:47 +0200

icingaweb2-module-director (1.6.0-2) unstable; urgency=medium

  * Updating debhelper.
  * Updating standard version.
  * Fixing typo in path (Closes: #952980).
  * Fixing typo in previous changelog entry.

 -- David Kunz <david.kunz@dknet.ch>  Mon, 22 Jun 2020 07:50:19 +0200

icingaweb2-module-director (1.6.0-1) unstable; urgency=medium

  * Initial release (Closes: #920422).

 -- David Kunz <david.kunz@dknet.ch>  Mon, 12 Aug 2019 15:15:36 +0200
